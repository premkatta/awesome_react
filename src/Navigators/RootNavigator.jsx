import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Screen1 from '../Screens/Feature1/Screen1';

const Stack = createNativeStackNavigator();

const RootNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Screen1" component={Screen1} />
    </Stack.Navigator>
  );
};

export default RootNavigator;
