import React from 'react';
import {View, Text, Button} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {decrement, incr_decr, increment} from './counterSlice';

export default () => {
  const dispatch = useDispatch();
  const counter_reducer = useSelector(store => store.counter);
  console.log(counter_reducer, 'counter_reducer==');
  const {count = 0} = counter_reducer;
  function increaseButton() {
    dispatch(increment(1));
  }

  function decreaseButton() {
    dispatch(decrement(1));
  }

  function incr_decrmButton(val) {
    dispatch(incr_decr(val));
  }

  return (
    <View>
      <Text style={{color: 'red'}}>{'Screen 1'}</Text>
      <Text style={{color: 'red'}}>{count}</Text>

      <View style={{marginTop: 10}}>
        <Button onPress={increaseButton} title={'increase1'} />
        <Button onPress={decreaseButton} title={'decrease'} />
        <Button onPress={() => incr_decrmButton(10)} title={'incr_dynamic'} />
        <Button onPress={() => incr_decrmButton(-10)} title={'decr_dynamic'} />
        <Text>{'branch added'}</Text>
      </View>
    </View>
  );
};
