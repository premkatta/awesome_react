import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  count: 0,
};

const counterSlice = createSlice({
  name: 'counter_slice',
  initialState,
  reducers: {
    increment: (state, action) => {
      state.count = state.count + action.payload;
    },
    decrement: (state, action) => {
      state.count = state.count - action.payload;
    },
    incr_decr: (state, action) => {
      state.count = state.count + action.payload * 1;
    },
  },
});

export const {increment, decrement, incr_decr} = counterSlice.actions;
export default counterSlice.reducer;
