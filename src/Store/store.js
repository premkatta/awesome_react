// with toolkit

import {configureStore} from '@reduxjs/toolkit';
import counterSlice from '../Screens/Feature1/counterSlice';

const store = configureStore({
  reducer: {
    counter: counterSlice,
  },
  devTools: process.env.NODE_ENV !== 'production',
  // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
});

export default store;
