import React, {useEffect} from 'react';
import RootNavigator from './src/Navigators/RootNavigator';
import {NavigationContainer} from '@react-navigation/native';
import {Provider} from 'react-redux';
import Config from 'react-native-config';
import store from './src/Store/store';
import NativeDevSettings from 'react-native/Libraries/NativeModules/specs/NativeDevSettings';
import {Text} from 'react-native';

const App2 = () => {
  const {ENV} = Config;

  // console.log(Config);
  // useEffect(() => {
  //   NativeDevSettings.setIsDebuggingRemotely(true);
  // }, []);
  // console.log(ENV, 'env');
  return (
    <NavigationContainer>
      <Provider store={store}>
        <Text style={{color: 'red'}}>{`${ENV}`}</Text>
        <RootNavigator />
      </Provider>
    </NavigationContainer>
  );
};

export default App2;
